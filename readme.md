# What is .NET core

.NET Core is a general purpose development platform maintained by Microsoft and the .NET community on GitHub. It is cross-platform, supporting Windows, macOS and Linux, and can be used in device, cloud, and embedded/IoT scenarios.

The following characteristics best define .NET Core:

- Flexible deployment: Can be included in your app or installed side-by-side user- or machine-wide.
- Cross-platform: Runs on Windows, macOS and Linux; can be ported to other OSes. The supported Operating Systems (OS), CPUs and application scenarios will grow over time, provided by Microsoft, other companies, and individuals.
- Command-line tools: All product scenarios can be exercised at the command-line.
- Compatible: .NET Core is compatible with .NET Framework, Xamarin and Mono, via the .NET Standard Library.
- Open source: The .NET Core platform is open source, using MIT an Apache 2 licenses. Documentation is licensed under CC-BY. .NET Core is a .NET Foundation project.
- Supported by Microsoft: .NET Core is supported by Microsoft, per .NET Core Support.

Basically, Microsoft built a version of .NET to allow developers to write cross-platform and cloud-optimized applications.

First, .NET is cross-platform. It runs on Windows, macOS and Linux, which allows the developer to share and run the exact same code between machines running different operating systems, with no changes in code and minimum or no changes in the development process. (Watch out for OS specific APIs!)

But the true innovation in .NET came with the modularization in design and architecture. The compiler (Roslyn) and the runtime (CoreCLR) are separate components that allow you to use different implementations (or even write your own).

Every library comes as a NuGet package, so when you start a new project, you don’t have any libraries, but a project file and a Program.cs. As you develop your app, you add libraries as you need them, allowing you to minimize the size of your application.

.NET Core also allows you to have multiple versions installed at the same time without having apps / parts of the OS breaking when you update or install a newer version of the framework, and will even allow you to ship the framework / parts of the framework with the application (since the footprint of the framework si small enough).

*.NET Core will favor performance with a standard library that minimizes allocations and the overall memory footprint of your system.*

## Versions

Versions of .NET Core available for download:

| Version      | Status      | Latest release | Latest release date | End of support |
|:------------:|:-----------:|:--------------:|:-------------------:|:--------------:|
|.NET Core 3.0 | Preview     | 3.0.0-preview7 | 2019-07-23          |                |
|.NET Core 2.2 | Current     | 2.2.6          | 2019-07-24          |                |
|.NET Core 2.1 | LTS         | 2.1.12         | 2019-07-24          |                |
|.NET Core 2.0 | End of life | 2.0.9          | 2018-07-10          | 2018-10-01     |
|.NET Core 1.1 | End of life | 1.1.13         | 2019-05-14          | 2019-06-27     |
|.NET Core 1.0 | End of life | 1.0.16         | 2019-05-14          | 2019-06-27     |

## How to install

- On windows download installer from [Microsoft](https://dotnet.microsoft.com/download/thank-you/dotnet-sdk-2.1.801-windows-x64-installer)
- On MacosX download installer from [Microsoft](https://dotnet.microsoft.com/download/thank-you/dotnet-sdk-2.1.801-macos-x64-installer)
- On linux:

    ```bash
    wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
    sudo dpkg -i packages-microsoft-prod.deb
    sudo add-apt-repository universe
    sudo apt-get install apt-transport-https
    sudo apt-get update
    sudo apt-get install dotnet-sdk-2.1
    ```

    If you receive an error message similar to Unable to locate package dotnet-sdk-2.1, run the following commands.

    ```bash
    sudo dpkg --purge packages-microsoft-prod && sudo dpkg -i packages-microsoft-prod.deb
    sudo apt-get update
    sudo apt-get install dotnet-sdk-2.1
    ```

    If that doesn't work, you can run a manual install with the following commands.

    ```bash
    sudo apt-get install -y gpg
    wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.asc.gpg
    sudo mv microsoft.asc.gpg /etc/apt/trusted.gpg.d/
    wget -q https://packages.microsoft.com/config/ubuntu/18.04/prod.list
    sudo mv prod.list /etc/apt/sources.list.d/microsoft-prod.list
    sudo chown root:root /etc/apt/trusted.gpg.d/microsoft.asc.gpg
    sudo chown root:root /etc/apt/sources.list.d/microsoft-prod.list
    sudo apt-get install -y apt-transport-https
    sudo apt-get update
    sudo apt-get install dotnet-sdk-2.1
    ```

## dotnet-cli

While visual studio is perfectly well with .NET core, some tasks can only executed in with command line. *for example creating native code*.

Popular commands:

- -h|--help: Show command, options and their meaning and parameters.
- --version: Show version of the dotnet
- new < template >: Create new something. This something will be indicated with name of the template.

  - --list: List all installed templates.
  - console: Console project.
  - classlib: .NETStandard class library project.
  - xunit: xUnit Test Project.
  - web: ASP.NET Core Empty.
  - ... and many many more.

- restore: restore nugets for the project and build it.
- build: build the ptoject you may use `-r < RID > -c < Configuration >` to build for an specific platform

## Topics

### ref struct

- Extension methods on ref structs

```cs --source-file code/ExtensionOnRefStructs/Point.cs --project code/ExtensionOnRefStructs/ExtensionOnRefStructs.csproj
```

```cs --region ExtensionOnStructs --source-file code/ExtensionOnRefStructs/Program.cs --project code/ExtensionOnRefStructs/ExtensionOnRefStructs.csproj
```

- System.Memory Nuget package and System.Buffer namespace:

  - Span< T >/ReadOnlySpan< T >:

    Is an array like type that allows representing managed and unmanaged memory in a uniform way and supports cheap slicing. It's at the heart of most performance-related improvements in .NET Core 2.1 and allows managing buffers in a more efficient way as it helps in reducing allocations and copying.
    *Span< T>* is considered a core type and requires runtime and compiler support in order to be fully leveraged. Please note that when we refer to *Span< T>* in this document, we really mean the family of span-related features which includes other types such as *ReadOnlySpan< T>*, *Memory< T>*, *ReadOnlyMemory< T>*, and the new *System.Buffers* namespace.
    It represent contiguous regions of arbitrary memory, with performance characteristics on par with *T[]*. Its APIs are similar to the array, but unlike arrays, it can point to either managed or native memory, or to memory allocated on the stack.

    ```csharp
    public ref struct Span<T> {
        public Span(T[] array);
        public Span(T[] array, int start, int length);
        public unsafe Span(void* pointer, int length);

        public static implicit operator Span<T> (ArraySegment<T> arraySegment);
        public static implicit operator Span<T> (T[] array);

        public int Length { get; }
        public ref T this[int index] { get; }

        public Span<T> Slice(int start);
        public Span<T> Slice(int start, int length);
        public bool TryCopyTo(Span<T> destination);

        public T[] ToArray();
    }
    ```

    It has two different implementations:

    - Fast Span< T> ( available on runtimes with special support for spans ):
      The fast implementation, will rely on "ref field" support and will look as follows:

      ```csharp
      public ref struct Span<T> {
          internal ref T _pointer;
          internal int _length;
      }
      ```

      A prototype of such fast *Span< T >* can be found here. Through the magic of the *ref field*, it can support slicing without requiring a strong pointer to the root of the sliced object. The **GC** is able to trace the interior pointer, keep the root object alive, and update the interior pointer if the object is relocated during a collection.

    - Slow Span< T> ( available on all current .NET runtimes, even existing ones, e.g. .NET 4.5 ):
      A different representation implemented for platforms that don’t support ref fields (interior pointers):

      ```csharp
      public struct Span<T> {
          internal IntPtr _pointer;
          internal object _relocatableObject;
          internal int _length;
      }
      ```

    **Problem: Struct Tearing**

    Struct tearing is a threading issue that affects all structs larger than what can be atomically updated on the target processor architecture. For example, some 64-bit processors can only update one 64-bit aligned memory block atomically. This means that some processors won’t be able to update both the _pointer and the _length fields of the Span atomically. This in turn means that the following code, might result in another thread observing _pointer and _length fields belonging to two different spans (the original one and the one being assigned to the field):

    ```csharp
    internal class Buffer {
        int _length;
        byte[] _memory;
        public Buffer(int cap) {
            Reset(new byte[cap]);
        }
        public Buffer(byte[] newMemory, int newLength) {
            Reset(newMemory, newLength);
        }

        public void Reset(byte[] newMemory, int newLength = 0) {
            // this update is not atomic. If atomicity is required, you must use some kind of lock
            _memory = newMemory;
            _length = newLength;
        }

        public int Size => _length;
        public int Capacity => _memory.Length;
        public byte this[int index] => _memory[index]; // this might see partial update
    }
    ```

    For most structs, tearing is at most a correctness bug and can be dealt with by making the fields (typed as the tearable struct type) non-public and synchronizing access to them. But since Span needs to be as fast as the array, access to the field cannot be synchronized. Also, because of the fact that Span accesses (and writes to) memory directly, having the _pointer and the _length be out of sync could result in memory safety being compromised.

    The only other way (besides synchronizing access, which would not be practical) to avoid this issue is to make Span a stack-only type, i.e. its instances can reside only on the stack (which is accessed by one thread).

    **Span< T> is stack-only**

    *Span< T>* is stack-only type; more precisely, it will be a by-ref type (just like its field in the fast implementation). This means that Spans cannot be boxed, cannot appear as a field of a non-stack-only type, and cannot be used as a generic argument. However, *Span< T>* can be used as a type of method arguments or return values.

    *Span< T>* being stack-only solves several problems:

    - Efficient representation and access: *Span< T>* can be just managed pointer and length.
    - Efficient GC tracking: limit number of interior pointers that the GC have to track. Tracking of interior pointers in the heap during GC would be pretty expensive.
    - Safe concurrency (struct tearing discussed above): *Span< T>* assignment does not have to be atomic. Atomic assignment would be required for storing *Span< T>* on the heap to avoid data tearing issues.
    - Safe lifetime: Safe code cannot create dangling pointers by storing it on the heap when *Span< T>* points to unmanaged memory or stack memory. The unsafe stack frame responsible for creating unsafe Span is responsible for ensuring that it won’t escape the scope.
    - Reliable buffer pooling: buffers can be rented from a pool, wrapped in spans, the spans passed to user code, and when the stack unwinds, the program can reliably return the buffer to the pool as it can be sure that there are no outstanding references to the buffer.

    The fast representation makes the type instances automatically stack-only, i.e. the constraint will be enforced by the CLR. This restriction should also be enforced by managed language compilers and/or analyzers for better developer experience. For the slow *Span< T>*, language compiler checks and/or analyzers is the only option (as the runtimes won't enforce the stack-only restriction).

  - Memory< T >/ReadOnlyMemory< T >:

    A stack-only type with the associated trade-offs is great for low level developers writing data transformation routines. Productivity developers, writing apps, may not be so thrilled when they realize that when using stack-only types, they lose many of the language features they rely on to get their jobs done (e.g. async/await). And so, a stack-only type simply can’t be the primary exchange type we recommend for high level developers/scenarios/APIs.

    For productivity developers we have *Memory< T>*, that can be used with the full power of the language, i.e. it’s not stack-only. *Memory< T>* can be seen as a “promise” of a Span. It can be freely used in generics, stored on the heap, used with async/await, and all the other language features we all love. When *Memory< T>* is finally ready to be manipulated by a data transformation routine, it will be temporarily converted to a span (the promise will be realized), which will provide much more efficient (remember "on par with array") access to the buffer's data.

    We have 2 kind of the *Memory< T>*:

    ```cs --region UseMemory --source-file code/Memory/Program.cs --project code/Memory/Memory.csproj
    ```

    - Unowned memory:

    ```cs --region UnownedMemory --source-file code/Memory/Program.cs --project code/Memory/Memory.csproj
    ```

    - Owned memory:

    ```cs --region OwnedMemory --source-file code/Memory/Program.cs --project code/Memory/Memory.csproj
    ```

    As you see in this examples memory that returned from [MemoryPool.Shared.Rent](https://docs.microsoft.com/en-us/dotnet/api/system.buffers.memorypool-1.rent?view=netstandard-2.1#System_Buffers_MemoryPool_1_Rent_System_Int32_) is not required to have exact length as requested. But it has at least requested length.
    So if you require exact length you must use [Slice](https://docs.microsoft.com/en-us/dotnet/api/system.memory-1.slice?view=netstandard-2.1#System_Memory_1_Slice_System_Int32_) method to get required slice of the memory.

    You may access items of this objects using item [Span](https://docs.microsoft.com/en-us/dotnet/api/system.memory-1.span?view=netstandard-2.1#System_Memory_1_Span) property.

    ```cs --region MemorySlice --source-file code/Memory/Program.cs --project code/Memory/Memory.csproj
    ```

  - ReadOnlySequence< T >:

    A sequence of *Memory< T >*

    ```cs --region WorkingWithSequences --source-file code/Sequence/Program.cs --project code/Sequence/Sequence.csproj
    ```

  - Tensor< T>:

    Tensor< T> can be thought of as extending *Memory< T>* for multiple dimensions and sparse data. Primary attributes for defining a *Tensor< T>* include:

    - Type T: Primitive (e.g. int, float, string…) and non-primitive types
    - Dimensions: The shape of the Tensor (e.g. 3×5 matrix, 28x28x3, etc.)
    - Layout (Optional): Row vs. Column major. The API calls this “ReverseStride” to generalize to many dimensions instead of just “row” and “column”. The default is reverseStride: false (i.e. row-major).
    - Memory (Optional): The backing storage for the values in the Tensor, which is used in-place without copying.

    ```csharp
    public static Tensor<float> ConvertImageToFloat(Bitmap bitmap)
    {
        Tensor<float> data = new DenseTensor<float>(new[] { bitmap.Width, bitmap.Height, 3 });
        for (int x = 0; x < bitmap.Width; x++)
        {
            for (int y = 0; y < bitmap.Height; y++)
            {
                Color color = bitmap.GetPixel(x, y);
                data[x, y, 0] = color.B;
                data[x, y, 1] = color.G;
                data[x, y, 2] = color.R;
            }
        }
        return data;
    }
    ```

### System.IO.Pipes

**Problem:** The problem with reading buffer of data from streams is that it is hard to implement correctly and if it is implemented correctly it is hard to understand.
For example consider we have an stream that we should read lines of data from it.

```csharp
async Task ProcessLinesAsync(Stream stream)
{
    var buffer = new byte[1024];
    await stream.ReadAsync(buffer, 0, buffer.Length);

    // Process a single line from the buffer
    ProcessLine(buffer);
}
```

This code might work when testing locally but it’s has several errors:

- The entire message (end of line) may not have been received in a single call to ReadAsync.
- It’s ignoring the result of stream.ReadAsync() which returns how much data was actually filled into the buffer.
- It doesn’t handle the case where multiple lines come back in a single ReadAsync call.

These are some of the common pitfalls when reading streaming data. To account for this we need to make a few changes:

- We need to buffer the incoming data until we have found a new line.
- We need to parse all of the lines returned in the buffer

So here's our new code:

```csharp
async Task ProcessLinesAsync(Stream stream)
{
    var buffer = new byte[1024];
    var bytesBuffered = 0;
    var bytesConsumed = 0;

    while (true)
    {
        var bytesRead = await stream.ReadAsync(buffer, bytesBuffered, buffer.Length - bytesBuffered);
        if (bytesRead == 0)
        {
            // EOF
            break;
        }
        // Keep track of the amount of buffered bytes
        bytesBuffered += bytesRead;

        var linePosition = -1;

        do
        {
            // Look for a EOL in the buffered data
            linePosition = Array.IndexOf(buffer, (byte)'\n', bytesConsumed, bytesBuffered - bytesConsumed);

            if (linePosition >= 0)
            {
                // Calculate the length of the line based on the offset
                var lineLength = linePosition - bytesConsumed;

                // Process the line
                ProcessLine(buffer, bytesConsumed, lineLength);

                // Move the bytesConsumed to skip past the line we consumed (including \n)
                bytesConsumed += lineLength + 1;
            }
        }
        while (linePosition >= 0);
    }
}
```

Once again, this might work in local testing but it’s possible that the line is bigger than 1KiB (1024 bytes). We need to resize the input buffer until we have found a new line.

Also, we’re allocating buffers on the heap as longer lines are processed. We can improve this by using the ArrayPool< byte> to avoid repeated buffer allocations as we parse longer lines from the client.

```csharp
async Task ProcessLinesAsync(NetworkStream stream)
{
    byte[] buffer = ArrayPool<byte>.Shared.Rent(1024);
    var bytesBuffered = 0;
    var bytesConsumed = 0;

    while (true)
    {
        // Calculate the amount of bytes remaining in the buffer
        var bytesRemaining = buffer.Length - bytesBuffered;

        if (bytesRemaining == 0)
        {
            // Double the buffer size and copy the previously buffered data into the new buffer
            var newBuffer = ArrayPool<byte>.Shared.Rent(buffer.Length * 2);
            Buffer.BlockCopy(buffer, 0, newBuffer, 0, buffer.Length);
            // Return the old buffer to the pool
            ArrayPool<byte>.Shared.Return(buffer);
            buffer = newBuffer;
            bytesRemaining = buffer.Length - bytesBuffered;
        }

        var bytesRead = await stream.ReadAsync(buffer, bytesBuffered, bytesRemaining);
        if (bytesRead == 0)
        {
            // EOF
            break;
        }

        // Keep track of the amount of buffered bytes
        bytesBuffered += bytesRead;

        do
        {
            // Look for a EOL in the buffered data
            linePosition = Array.IndexOf(buffer, (byte)'\n', bytesConsumed, bytesBuffered - bytesConsumed);

            if (linePosition >= 0)
            {
                // Calculate the length of the line based on the offset
                var lineLength = linePosition - bytesConsumed;

                // Process the line
                ProcessLine(buffer, bytesConsumed, lineLength);

                // Move the bytesConsumed to skip past the line we consumed (including \n)
                bytesConsumed += lineLength + 1;
            }
        }
        while (linePosition >= 0);
    }
}
```

This code works but now we’re re-sizing the buffer which results in more buffer copies. It also uses more memory as the logic doesn’t shrink the buffer after lines are processed. To avoid this, we can store a list of buffers instead of resizing each time we cross the 1KiB buffer size.

Also, we don’t grow the the 1KiB buffer until it’s completely empty. This means we can end up passing smaller and smaller buffers to ReadAsync which will result in more calls into the operating system.

To mitigate this, we’ll allocate a new buffer when there’s less than 512 bytes remaining in the existing buffer:

```csharp
public class BufferSegment
{
    public byte[] Buffer { get; set; }
    public int Count { get; set; }

    public int Remaining => Buffer.Length - Count;
}

async Task ProcessLinesAsync(NetworkStream stream)
{
    const int minimumBufferSize = 512;

    var segments = new List<BufferSegment>();
    var bytesConsumed = 0;
    var bytesConsumedBufferIndex = 0;
    var segment = new BufferSegment { Buffer = ArrayPool<byte>.Shared.Rent(1024) };

    segments.Add(segment);

    while (true)
    {
        // Calculate the amount of bytes remaining in the buffer
        if (segment.Remaining < minimumBufferSize)
        {
            // Allocate a new segment
            segment = new BufferSegment { Buffer = ArrayPool<byte>.Shared.Rent(1024) };
            segments.Add(segment);
        }

        var bytesRead = await stream.ReadAsync(segment.Buffer, segment.Count, segment.Remaining);
        if (bytesRead == 0)
        {
            break;
        }

        // Keep track of the amount of buffered bytes
        segment.Count += bytesRead;

        while (true)
        {
            // Look for a EOL in the list of segments
            var (segmentIndex, segmentOffset) = IndexOf(segments, (byte)'\n', bytesConsumedBufferIndex, bytesConsumed);

            if (segmentIndex >= 0)
            {
                // Process the line
                ProcessLine(segments, segmentIndex, segmentOffset);

                bytesConsumedBufferIndex = segmentOffset;
                bytesConsumed = segmentOffset + 1;
            }
            else
            {
                break;
            }
        }

        // Drop fully consumed segments from the list so we don't look at them again
        for (var i = bytesConsumedBufferIndex; i >= 0; --i)
        {
            var consumedSegment = segments[i];
            // Return all segments unless this is the current segment
            if (consumedSegment != segment)
            {
                ArrayPool<byte>.Shared.Return(consumedSegment.Buffer);
                segments.RemoveAt(i);
            }
        }
    }
}

(int segmentIndex, int segmentOffest) IndexOf(List<BufferSegment> segments, byte value, int startBufferIndex, int startSegmentOffset)
{
    var first = true;
    for (var i = startBufferIndex; i < segments.Count; ++i)
    {
        var segment = segments[i];
        // Start from the correct offset
        var offset = first ? startSegmentOffset : 0;
        var index = Array.IndexOf(segment.Buffer, value, offset, segment.Count - offset);

        if (index >= 0)
        {
            // Return the buffer index and the index within that segment where EOL was found
            return (i, index);
        }

        first = false;
    }
    return (-1, -1);
}
```

This code just got much more complicated. We’re keeping track of the filled up buffers as we’re looking for the delimiter. To do this, we’re using a List< BufferSegment> here to represent the buffered data while looking for the new line delimiter. As a result, ProcessLine and IndexOf now accept a List< BufferSegment> instead of a byte[], offset and count. Our parsing logic needs to now handle one or more buffer segments.

Our server now handles partial messages, and it uses pooled memory to reduce overall memory consumption but there are still a couple more changes we need to make:

- The byte[] we’re using from the ArrayPool< byte> are just regular managed arrays. This means whenever we do a ReadAsync or WriteAsync, those buffers get pinned for the lifetime of the asynchronous operation (in order to interop with the native IO APIs on the operating system). This has performance implications on the garbage collector since pinned memory cannot be moved which can lead to heap fragmentation. Depending on how long the async operations are pending, the pool implementation may need to change.
- The throughput can be optimized by decoupling the reading and processing logic. This creates a batching effect that lets the parsing logic consume larger chunks of buffers, instead of reading more data only after parsing a single line. This introduces some additional complexity:

  - We need two loops that run independently of each other. One that reads from the Socket and one that parses the buffers.
  - We need a way to signal the parsing logic when data becomes available.
  - We need to decide what happens if the loop reading from the Socket is “too fast”. We need a way to throttle the reading loop if the parsing logic can’t keep up. This is commonly referred to as “flow control” or “back pressure”.
  - We need to make sure things are thread safe. We’re now sharing a set of buffers between the reading loop and the parsing loop and those run independently on different threads.
  - The memory management logic is now spread across two different pieces of code, the code that rents from the buffer pool is reading from the socket and the code that returns from the buffer pool is the parsing logic.
  - We need to be extremely careful with how we return buffers after the parsing logic is done with them. If we’re not careful, it’s possible that we return a buffer that’s still being written to by the Socket reading logic.

The complexity has gone through the roof (and we haven’t even covered all of the cases). High performance networking usually means writing very complex code in order to eke out more performance from the system.

The goal of *System.IO.Pipelines* is to make writing this type of code easier.

**Solution:** Using *System.IO.Pipelines* we rewrite the code:

```cs --region Code --source-file code/Pipelines/Program.cs --project code/Pipelines/Pipelines.csproj
```
