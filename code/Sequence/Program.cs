﻿using System;
using System.Buffers;

namespace Dotnet.Core.Learning.WorkingWithSequence
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            #region WorkingWithSequences
            var array1 = new int[] { 1, 2, 3, 4 };
            var array2 = new int[] { 5, 6, 7, 8 };
            var array3 = new int[] { 9, 10, 11, 12 };
            (var first, var last) = SequenceSegment<int>.FromCollection(
                new Memory<int>(array1),
                new Memory<int>(array2),
                new Memory<int>(array3)
            );

            var sequence = new ReadOnlySequence<int>(first, 0, last, last.Memory.Length);
            foreach (var memory in sequence)
            {
                foreach (var item in memory.Span)
                {
                    Console.Write("{0} ", item);
                }
            }
            Console.WriteLine();

            SequencePosition? n = sequence.PositionOf(5);
            if (n is null)
            {
                Console.WriteLine("Couldn't find the item");
            }
            else
            {
                var position = n.Value;
                var memory = sequence.Slice(position).First;
                Console.WriteLine("Found item: {0}", memory.Span[0]);
            }
            #endregion
        }
    }
}
