﻿using System;

namespace Dotnet.Core.Learning.ExtensionOnRefStructs
{
    class Program
    {
        static void Main(string[] args)
        {
            #region ExtensionOnStructs
            Point point = new Point {
                X = 10,
                Y = 100,
            };
            point.ChangeX(100);
            Console.WriteLine("In previous C# we couldn't write an extension on struct that can modify it");
            Console.WriteLine("But now we have: point: ({0}, {1})", point.X, point.Y);
            #endregion
        }
    }
}
