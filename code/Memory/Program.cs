﻿using System;
using System.Buffers;

namespace Dotnet.Core.Learning.WorkingWithMemory
{
    internal class Program
    {
        #region UseMemory
        public static void UseMemory(Memory<byte> buffer)
        {
            Console.WriteLine("Size of received memory is {0}", buffer.Length);
            for (int i = 0; i < buffer.Length; i++)
            {
                buffer.Span[i] = unchecked((byte)i);
            }
        }
        #endregion

        public static void Main(string[] args)
        {
            #region UnownedMemory
            Console.WriteLine("Working with unowned memory");
            var memory = new Memory<byte>(new byte[100]);
            UseMemory(memory);
            Console.WriteLine();
            #endregion

            #region OwnedMemory
            Console.WriteLine("Working with owned memory");
            using (var memoryOwner = MemoryPool<byte>.Shared.Rent(100))
            {
                UseMemory(memoryOwner.Memory);
            }
            Console.WriteLine();
            #endregion

            #region MemorySlice
            Console.WriteLine("As you can see, size of the memory that received from MemoryPool may be different from the requested size.");
            Console.WriteLine("So if you want to have exact length you must use Slice method:");
            using (var memoryOwner = MemoryPool<byte>.Shared.Rent(100))
            {
                var slicedMemory = memoryOwner.Memory[0..100];
                UseMemory(slicedMemory);
            }
            Console.WriteLine();
            #endregion
        }
    }
}
